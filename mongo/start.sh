#!/bin/bash

#Variables
mongoUser="mongo"
mongoPass="test1234"

#Build image from Dockerfile
sudo docker build -t mongo_miguel:test .

sleep 10

#Launch image
sudo docker run --name mongo-miguel \
    -e MONGO_INITDB_ROOT_USERNAME=$mongoUser \
    -e MONGO_INITDB_ROOT_PASSWORD=$mongoPass \
    -p 27017:27017 \
    -d mongo_miguel:test
    
sleep 10    

#Import dataset
sudo docker exec -it $(sudo docker ps -aqf "name=mongo-miguel") mongoimport --db test --collection restaurant --authenticationDatabase admin --username $mongoUser --password $mongoPass --drop --file /var/tmp/restaurant.json

