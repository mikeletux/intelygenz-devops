FROM ubuntu:latest

ENV MONGO_URI mongodb://mongo:test1234@localhost:27017/test?authSource=admin

RUN apt-get update && \
    apt-get install -y \
    python3 \
    python3-pip

COPY . /opt/app

RUN pip3 install -r /opt/app/requirements.txt

ENTRYPOINT python3 /opt/app/app.py